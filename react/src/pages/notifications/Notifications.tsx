import React, { Component } from 'react';
import { Button, Row, Col } from 'antd/es';
import Table from './components/Table';
import Drawer from './components/Drawer';
import NotificationsService from './NotificationsService';
interface IState {
    visible: boolean;
    notification: any;
}
export default class Notifications extends Component< {}, IState> {
    state = { 
        visible: false, 
        notification: {
        titulo:'',
        descripcion: ''
    } };
    componentDidMount() {
        NotificationsService.toggleDrawer.subscribe(() => {
            this.setState(state => ({ visible: !state.visible }));
        });
    }
    newClicked() {
        const { notification } = this.state;
        NotificationsService.itemSelected.next({notification});
        NotificationsService.toggleDrawer.next();
    }
    render() {
        return (
            <Row>
                <Row justify="end" type="flex" style={{paddingBottom:10}}>
                    <Col>
                        <Button onClick={() => this.newClicked()} type="primary" >Nuevo</Button>
                    </Col>
                </Row>
                <Row>
                    <Table />
                </Row>
            <Drawer visible={this.state.visible}/>
            </Row>
        )
    }
}
