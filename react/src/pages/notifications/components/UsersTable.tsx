import React, { Component } from 'react';
import { Input } from 'antd';
import axios from '../../../api/axios';
import { Row, Button, Table, message } from 'antd/es';
import NotificationsService from '../NotificationsService';
const { Search } = Input;

export default class UsersTable extends Component {
    state = {
        data: [],
        loading: false,
        usersSelection: [],
        notification: {id: '', titulo:'', descripcion: ''},
        search: ''
    }
    columns = [
        {
          title: 'Matricula',
          dataIndex: 'matricula',
          width: '30%',
          filters: []
        },
        {
          title: 'Nombre',
          dataIndex: 'name_user',
          width: '70%',
        },
      ];
      componentDidMount() {
          this.fetch();
      }
      componentDidUpdate(){
          console.log('componentDidUpdate');
          const { notification } = NotificationsService;
          if(notification.id !== this.state.notification.id) {
            this.setState({notification});
          }
      }
      fetch = (params = {}) => {
        this.setState({ loading: true });
        const { search } = this.state;
        axios.get('users', { params: { search }}).then(response => {
            //const pagination = { ...this.state.pagination };
            // Read total count from server
            // pagination.total = data.totalCount;
            //pagination.total = 200;
            this.setState({
              loading: false,
              data: response.data,
              //pagination,
            });
          });
      };
    onSelectChange = (_: any[], usersObject: any) => {
        const usersSelection = usersObject.map((u: any) => u.id);
        console.log('selectedRowKeys changed: ', usersSelection);
        this.setState({ usersSelection });
    };

    onSearch(search: string) {
        this.setState({search}, () => {
            this.fetch();
        });
    }
    async sendNotifications() {
       try {
        const { usersSelection, notification } = this.state;
         const response = await axios.post('notifications/send',{
            ids: usersSelection,
            notification
        });
        if (response) {
            message.success('Notificaciones enviadas correctamente');
        }
       } catch(e) {
           console.log(e);
       }
    }
    render() {
        const { usersSelection, notification } = this.state;
        const rowSelection: any = {
            selectedRowKeys: usersSelection,
            onChange: this.onSelectChange
          };
        return (
            <Row>
                <Row style={style}>
                    <p>{notification.titulo}</p>
                    <span>{notification.descripcion}</span>
                </Row>
                <Search
                    style={style}
                    placeholder="Ingrese matricula o nombre del Alumno"
                    onSearch={this.onSearch.bind(this)} enterButton />
                    <br/>
                <Table
                    columns={this.columns}
                    rowKey={(record: any) => record.id}
                    bordered
                    dataSource={this.state.data}
                    loading={this.state.loading}
                    rowSelection={rowSelection}
                    // onChange={this.handleTableChange}
                />
                <Row type="flex" justify="center">
                    <Button type="primary" onClick={this.sendNotifications.bind(this)}>Enviar notificaciones</Button>
                </Row>
            </Row>
        )
    }
}
const style = {
    paddingBottom: 10
}
