import React, { Component } from 'react'
import { Drawer, Form, Button, Col, Row, Input, message } from 'antd';
import axios from '../../../api/axios';
import NotificationsService from '../NotificationsService';
import UsersTable from './UsersTable';
interface DrawerFormProps {
    visible: boolean;
}
interface IState {
    notification: {
        id?: string;
        titulo?: string;
        descripcion?: string;
    };
    visible: boolean;
    showUsersTable: boolean;
}
class DrawerForm extends Component<DrawerFormProps, IState> {
  state = { 
    visible: false,
    notification: {id: '', titulo: '', descripcion: ''},
    showUsersTable: false
  };
  componentDidMount() {
      NotificationsService.itemSelected.subscribe(({notification, showUsersTable}) => {
          this.setState({
            notification,
            showUsersTable: !!showUsersTable
          });
      })
  }
  onClose = () => {
    NotificationsService.toggleDrawer.next();
  };
  updateField(field: string, value: string) {
      this.setState(state => ({
        notification: {
            ...state.notification,
            [field]: value
        }
    }));
  }

  async submitNotification() {
   try {
    const { notification } = this.state;
    const { titulo, descripcion, id } = notification;
    if (!id) {
       await axios.post('notifications', { title: titulo, description: descripcion });
    }
    else {
        await axios.put('notifications', { title: titulo, description:descripcion, id });
    }
        message.success('Notificacion guardada correctamente');
        NotificationsService.itemSubmited.next();
        this.onClose();
   } catch(e) {
       console.log(e);
    message.error('Error al guardar la notificacion');
   }
  }
  FormContent = () => (<Row>
    <Form layout="vertical" hideRequiredMark>
        <Row gutter={8}>
          <Col span={24}>
            <Form.Item label="Titulo">
                <Input 
                  placeholder="Titulo" 
                  value={this.state.notification.titulo}
                  onChange={(input) => this.updateField('titulo', input.currentTarget.value)}
                  />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8}>
          <Col span={24}>
            <Form.Item label="Descripcion">
                <Input.TextArea 
                    rows={4} 
                    placeholder="Descripcion" 
                    value={this.state.notification.descripcion}
                    onChange={(input) => this.updateField('descripcion', input.currentTarget.value)}
                />
            </Form.Item>
          </Col>
        </Row>
      </Form>
        <Row type="flex" justify="end">
        <Button onClick={this.onClose} style={{ marginRight: 8 }}>
          Cancelar
        </Button>
        <Button onClick={this.submitNotification.bind(this)} type="primary">
          Enviar
        </Button>
        </Row>
   </Row>);
  render() {
      const { visible } = this.props;
      const { showUsersTable } = this.state;
    return (
      <Drawer
      title="Enviar notificaciones"
      width={560}
      onClose={this.onClose}
      visible={visible}
    >
      { showUsersTable ? <UsersTable/> : <this.FormContent/> }
        </Drawer>
    );
  }
}
export default DrawerForm;