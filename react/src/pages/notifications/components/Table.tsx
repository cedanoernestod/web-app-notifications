import React, { Component } from 'react'
import axios from '../../../api/axios';
import { Table, Icon } from 'antd';
import  NotificationsService from '../NotificationsService';


interface TableComponentProps {
}
export default class TableComponent extends Component<TableComponentProps> {
  columns = [
    {
      title: 'Titulo',
      dataIndex: 'titulo',
      width: '20%',
    },
    {
      title: 'Descripcion',
      dataIndex: 'descripcion',
      width: '80%',
    },
    {
      title:'',
      render: (u: any) => <Icon type="mail" theme="twoTone" onClick={(e) => this.clickNotification(e, u)} />
    }
  ];
  state = {
    data: [],
    pagination: {},
    loading: false,
  };

  componentDidMount() {
    this.fetch();
    NotificationsService.itemSubmited.subscribe(() => {
      this.fetch();
    })
  }
  clickNotification(event: any, notification: any) {
    event.stopPropagation();
    NotificationsService.itemSelected.next({notification, showUsersTable: true});
    NotificationsService.notification = notification;
    NotificationsService.toggleDrawer.next();
  }
//   handleTableChange = (pagination, filters, sorter) => {
//     const pager = { ...this.state.pagination };
//     pager.current = pagination.current;
//     this.setState({
//       pagination: pager,
//     });
//     this.fetch({
//       results: pagination.pageSize,
//       page: pagination.current,
//       sortField: sorter.field,
//       sortOrder: sorter.order,
//       ...filters,
//     });
//   };

  fetch = (params = {}) => {
    this.setState({ loading: true });
    axios.get('notifications').then(response => {
        //const pagination = { ...this.state.pagination };
        // Read total count from server
        // pagination.total = data.totalCount;
        //pagination.total = 200;
        this.setState({
          loading: false,
          data: response.data,
          //pagination,
        });
      });
  };
  itemSelected(notification: any) {
    NotificationsService.toggleDrawer.next();
    NotificationsService.itemSelected.next({notification});
  }

  render() {
    return (
      <Table
        columns={this.columns}
        // rowKey={record => record.login.uuid}
        bordered
        dataSource={this.state.data}
        loading={this.state.loading}
        onRowClick={(notification) => this.itemSelected(notification)}
        // onChange={this.handleTableChange}
      />
    );
  }
}