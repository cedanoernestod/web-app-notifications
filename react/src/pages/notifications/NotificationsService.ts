import { Subject } from 'rxjs';
export default class {
    private static  _notification: any;
    static set notification(notification: any) {
        this._notification = notification;
    }
    static get notification() {
        return this._notification;
    }
    static itemSelected =  new Subject<any>();
    static itemSubmited = new Subject<void>();
    static toggleDrawer = new Subject<void>();
}
