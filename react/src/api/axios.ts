import axios from 'axios';

export default axios.create({
    baseURL: 'http://138.68.60.91:5000/',
    headers: {
        'x-auth-token': `eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjoxNDAxLCJwYXNzd29yZCI6IjA5OGY2YmNkNDYyMWQzNzNjYWRlNGU4MzI2MjdiNGY2IiwiaWF0IjoxNTY3MTczMTc2fQ.yRHUjLub8TnmJZiN2o7ZUc7EwylBaFmbqoWLuazmSOw`
    }
});