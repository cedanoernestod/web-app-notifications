import React from 'react';
import Notifications from './pages/notifications/Notifications';
import './App.css';
import { Layout, Menu, Breadcrumb } from 'antd';
const { Header, Content, Footer } = Layout;

const App: React.FC = () => {
  return (
    <Layout style={{height: '100%'}}>
    <Header style={{ position: 'fixed', zIndex: 1, width: '100%' }}>
      <div className="logo" />
      <Menu
        theme="dark"
        mode="horizontal"
        defaultSelectedKeys={['1']}
        style={{ lineHeight: '64px' }}
      >
        <Menu.Item key="1">Notificaciones</Menu.Item>
      </Menu>
    </Header>
    <Content style={{ padding: '0 50px', marginTop: 64, overflow: 'initial', minHeight: '100%' }}>
      <Breadcrumb style={{ margin: '16px 0' }}>
        <Breadcrumb.Item>Sistema</Breadcrumb.Item>
        <Breadcrumb.Item>Notificaciones</Breadcrumb.Item>
      </Breadcrumb>
      <div >
        <Notifications/>
      </div>
    </Content>
    <Footer style={{ textAlign: 'center' }}></Footer>
  </Layout>
  
  );
}

export default App;
