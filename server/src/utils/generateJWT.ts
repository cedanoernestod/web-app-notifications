import config from 'config';
import jwt from 'jsonwebtoken';
export default (user: any) => {
    const { id_user, password } = user;
    const token = jwt.sign({id_user, password}, config.get('myprivatekey'));
    return token;
}