import jwt from 'jsonwebtoken';
import config from 'config';
import { Request, Response, NextFunction } from 'express';

export default (req: any, res: Response, next: NextFunction) => {
  //get the token from the header if present
  const token: any = req.headers["x-auth-token"] || req.headers["authorization"];
  //if no token found, return response (without going to the next middelware)
  if (!token)  res.status(401).send("Access denied. No token provided.");
  try {
    // if can verify the token, set req.user and pass to next middleware
    const decoded = jwt.verify(token, config.get("myprivatekey"));
    req.user = decoded;
    next();
  } catch (ex) {
    res.status(401).send("Invalid token");
  }
};