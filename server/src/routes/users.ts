import { auth, getUsers  } from '../controllers/users';
import authMiddleware from '../middleware/auth';
import { Router } from 'express';
const router = Router();
router.get('/auth', auth);
router.get('/', authMiddleware, getUsers);
export default router; 
