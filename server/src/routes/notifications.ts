import { post, get, put, send, getByUser  } from '../controllers/notifications';
import authMiddleware from '../middleware/auth';
import { Router } from 'express';
export default (io: any) => {
    const router = Router();
    router.post('/', authMiddleware,post);
    router.post('/send', authMiddleware, send(io));
    router.put('/', authMiddleware, put);
    router.get('/', authMiddleware, get);
    router.get('/user/:userId', authMiddleware, getByUser);
    return router;
} 
