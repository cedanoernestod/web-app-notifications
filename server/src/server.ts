import express from 'express';
import bodyParser from 'body-parser';
import usersRoute  from './routes/users';
import notificationsRoutes  from './routes/notifications';
import Mysql from './config/database';
const app= express();
const server = require('http').Server(app);
const db  = new Mysql();
const io = require('socket.io')(server);
server.listen(5000);
db.connect();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "Origin, x-auth-token, X-Requested-With, Content-Type, Accept");
    next();
});

  
app.use('/users', usersRoute);
app.use('/notifications', notificationsRoutes(io));
io.on('connection', (socket: any) => {
    console.log('a user connected');
});