const mysql = require('mysql');
class Mysql {
  public static connection = mysql.createConnection({
    host     : 'mysql',
    user     : 'root',
    password : 'users192837465',
    database : 'users'
  });
  
  connect () {
    Mysql.connection.connect((err: Error) => {
        if (err) {
          console.error('error connecting: ' + err.stack);
          return;
        }
        console.log('connected as id ' + Mysql.connection.threadId);
    });
  }

}
export default Mysql;