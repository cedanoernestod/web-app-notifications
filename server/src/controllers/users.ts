import Mysql from '../config/database';
import generateJWT from '../utils/generateJWT';
import { Request, Response } from 'express';
export const auth = (req: Request, res: Response) => {
    const { username, password } = req.query;
    Mysql.connection.query('SELECT id_user AS id FROM usuarios WHERE username = ? AND password = md5(?)',
    [username, password],(errors: any, result: any, fields: any) => {
        if (errors) {
            res.json({errors});
        } else {
            if (result && result.length === 1) {
                const user =  result.find((u: any) => u);
                const token = generateJWT(user);
                if (!req.headers["x-access-token"]) {
                    res.header("x-auth-token", token);
                    res.json(user);
                }
                res.status(200);
                return;
            }
            res.status(204);
            res.json({ message: 'User not found' });
        }
    });
}

export const getUsers = (req: Request, res: Response) => {
    const { search } = req.query;
    Mysql.connection.query(`SELECT id_user as id, name_user, matricula FROM usuarios WHERE name_user LIKE '%${search}%' OR matricula  LIKE '%${search}%'`, (errors: any, result: any, fields: any) => {
        if (!errors) {
            res.status(200);
            res.json(result);
        }
    });
}