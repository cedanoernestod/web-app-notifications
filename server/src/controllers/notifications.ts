import Mysql from '../config/database';
import { Request, Response } from 'express';
import notifications from '../routes/notifications';
export const post = (req: Request, res: Response) => {
    const { title, description } = req.body;
    Mysql.connection.query('INSERT INTO notificaciones SET ?',
    { titulo: title, descripcion: description },(errors: any, result: any, fields: any) => {
        if (!errors) {
            res.status(204);
            res.json();
        } else {
            res.status(400);
            res.json({errors}); 
        }
    });
}
export const put = (req: Request, res: Response) => {
    const { title, description, id } = req.body;
    Mysql.connection.query('UPDATE notificaciones SET titulo = ?, descripcion = ? WHERE id = ?',
    [title, description, id],(errors: any, result: any, fields: any) => {
        if (errors) {
            res.json({errors});
            res.status(400);
        } else {
            if (result) {
                res.status(204);
                res.json();
            }
        }
    });
}

export const get = (req: Request, res: Response) => {
    const { userId} = req.query;
    Mysql.connection.query('SELECT * FROM notificaciones LIMIT 100',
    (errors: any, result: any, fields: any) => {
        if (!errors) {
            res.json(result);
        }
    });
}
export const getByUser = (req: Request, res: Response) => {
    const { userId } = req.params;;
    Mysql.connection.query(`SELECT n.titulo, n.descripcion, nu.enviado
    FROM notificaciones n
    INNER JOIN notificaciones_usuarios nu ON n.id = nu.id_notificacion
    WHERE nu.id_usuario = "${userId}"`,
    (errors: any, result: any, fields: any) => {
        if (!errors) {
            res.json(result);
        }
    });
}
export const send = (io: any) => (req: Request, res: Response) => {
    try {
        let notificationsSent = 0;
        const { notification, ids } = req.body;
        for (const userId of ids) {
            Mysql.connection.query('INSERT INTO notificaciones_usuarios SET ?',
            { id_usuario: userId, id_notificacion: notification.id},(errors: any, result: any, fields: any) => {
                if (!errors) {
                    io.emit('notification', {
                        userId,
                        notification
                    });
                    notificationsSent++;
                } else {
                    res.json(errors);
                }
            });
        }
            res.status(204);
            res.json([]);
    } catch(e) {
        res.status(400);
    }
    //io.emit('notifications', req.body)

}