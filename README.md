**Descripcion del projecto y tecnologias**
Este repositorio contiene la aplicacion de Backend desarrollada con un microservicio de `NodeJS`, utilizando `Express` como framework, `JWT` para la seguridad de los endpoints y `socket-io`
Para la comunicacion de servidor -> aplicacion movil

**Directorios**
*  `build` Codigo de Javascript transpilado
*  `client` Contiene el codigo de React para el frontend
*  `config` archivos de configuracion para el proyecto (JWT, BD)
*  `node_modules` dependencias del proyecto
*  `src` Codigo de la aplicacion 
*  

**Conexion a la base de datos**
Reemplazar la conexion a la base de datos por la correcta `src/config/database.ts`

Se tiene que considerar remplazar las URLs por las que IP que se obtenga una vez que este servicio este en los servidores, en los siguientes archivos:
   `client/src/api/axios.ts`
   
  **Ejecutar aplicacion**
  
*  Primero se tiene que tener instalado `nodejs` [https://nodejs.org/en/](https://nodejs.org/en/)
*  Se tienen que instalar las dependencias del proyecto, navegar al directorio raiz de este repositorio y ejecutar `npm install`
*  Una vez finalizada la instalacion ejecutar `npm run app` este comando iniciara en Frontend en http://localhost:3000/ y el backend en http://localhost:5000/
  


