FROM node:latest
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY ./react/package*.json /usr/src/app/
EXPOSE 3000
CMD /bin/bash -c 'npm install; npm start'